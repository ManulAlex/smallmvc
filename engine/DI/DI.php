<?php

declare(strict_types=1);

namespace Engine\DI;

class DI implements DiInterface
{
    /**
     * @var array
     */
    private array $container = [];

    /**
     * @param $key
     * @param $value
     * @return DI
     */
    public function set($key, $value)
    {
        $this->container[$key] = $value;

        return $this;
    }

    /**
     * @param $key
     * @return mixed
     *
     */
    public function get($key)
    {
        return $this->has($key);
    }

    /**
     * Возвращает булево значение, возможно можно сделать приватным
     * @param $key
     * @return bool
     */
    public function has($key)
    {
        return isset($this->container[$key]) ? $this->container[$key] : null;
    }
}