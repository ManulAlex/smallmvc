<?php

declare(strict_types=1);

namespace Engine\DI;

interface DiInterface
{
    public function set($key, $value);

    public function get($key);
}