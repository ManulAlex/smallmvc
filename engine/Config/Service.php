<?php

declare(strict_types=1);

return [
    Engine\Service\Database\Provider::class,
    Engine\Service\Router\Provider::class,
];