<?php

declare(strict_types=1);

namespace Engine\Exception;

use Exception;

/**
 * Class StartFailException
 * @package Engine\Exception
 */
class StartFailException extends Exception
{
    /**
     * @return StartFailException
     */
    public static function withStart()
    {
        return new StartFailException('StartFailed');
    }
}