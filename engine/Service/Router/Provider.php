<?php

declare(strict_types=1);

namespace Engine\Service\Router;

use Engine\Service\AbstractProvider;
use Engine\Core\Router\Router;

/**
 * Class Provider
 * @package Engine\Service\Database
 */
class Provider extends AbstractProvider
{
    /**
     * @var string
     */
    public string $serviceName = 'router';
    /**
     * @return mixed|void
     */
    public function init()
    {
        $router = new Router('http://devscms');

        $this->di->set($this->serviceName, $router);
    }
}