<?php

declare(strict_types=1);

namespace Engine;

use Engine\Core\Router\Router;
use Engine\DI\DI;

class Cms
{
    /**
     * @var DI
     */
    private DI $di;

    /**
     * @var bool|mixed|null
     */
    public $router;

    /**
     * Cms constructor.
     * @param DI $di
     */
    public function __construct(DI $di)
    {
        $this->di = $di;
        $this->router = $this->di->get('router');
    }

    /**
     *
     */
    public function run()
    {
       $this->router->add('home', '/', 'HomeController:index');
       print_r($this->di);
    }
}